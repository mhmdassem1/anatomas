﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Experimental.XR;
using System;
using Photon.Realtime;
using Photon.Pun;
using UnityEngine.UI;

public class ARTapToPlaceObject : MonoBehaviour
{
    public GameObject objectToPlace;
    public GameObject placementIndicator;

    private ARRaycastManager arOrigin;
    private Pose placementPose;
    private bool placementPoseIsValid;
    private bool boardIsPlaced;
    private bool placedFirstTime;
    private GameObject instantiatedPrefab;


    private float distanceBetweenBoxAndCam;
    Vector3 Asiobia = new Vector3(100000, 0, 0);

    Button Show;
    Button Hide;
    private void Awake()
    {
        Show = GameObject.FindGameObjectWithTag("Show").GetComponent<Button>();
        Hide = GameObject.FindGameObjectWithTag("Hide").GetComponent<Button>();
    }

    void Start()
    {
        /// the buttons/
        Hide.gameObject.SetActive(false);
        Show.gameObject.SetActive(false);
        arOrigin = FindObjectOfType<ARRaycastManager>();
        placementPoseIsValid = false;
        boardIsPlaced = false;
        placedFirstTime = false;
        placementIndicator.SetActive(false);
        SetAllPlanesActive(true);

#if UNITY_EDITOR
        PlaceObject();
        gameObject.SetActive(false);
#endif

    }

    void Update()
    {
        if (!boardIsPlaced)
        {
            UpdatePlacementPose();
            UpdatePlacementIndicator();
        }
        else
        {
            instantiatedPrefab.transform.position = placementPose.position;
        }

        if (placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && !boardIsPlaced)
        {
            placementIndicator.SetActive(false);
            PlaceObject();
            SetAllPlanesActive(false);
        }
    }

    private void PlaceObject()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            Hide.gameObject.SetActive(true);
            Show.gameObject.SetActive(true);
        }

        if (!placedFirstTime)
        {  
            placedFirstTime = true;
            instantiatedPrefab = Instantiate(objectToPlace, placementPose.position, placementPose.rotation);
            boardIsPlaced = true;
            distanceBetweenBoxAndCam = Vector3.Distance(instantiatedPrefab.transform.position, Camera.main.transform.position);
            Debug.Log(distanceBetweenBoxAndCam + "____________________________________ _l_");
        }
        else
        {
            instantiatedPrefab.transform.position = placementPose.position;
            instantiatedPrefab.transform.rotation = placementPose.rotation;
            //instantiatedPrefab.SetActive(true);
            boardIsPlaced = true;

        }

    }

    private void UpdatePlacementIndicator()
    {
        if (placementPoseIsValid)
        {
            placementIndicator.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        }
        else
        {
            placementIndicator.SetActive(false);
        }
    }

    private void UpdatePlacementPose()
    {
        var screenCenter = Camera.main.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        arOrigin.Raycast(screenCenter, hits, UnityEngine.XR.ARSubsystems.TrackableType.Planes);

        placementPoseIsValid = hits.Count > 0;
        if (placementPoseIsValid)
        {
            placementPose = hits[0].pose;

            var cameraForward = Camera.main.transform.forward;
            var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
            placementPose.rotation = Quaternion.LookRotation(cameraBearing);
        }
    }

    public void ResetAR()
    {
        boardIsPlaced = false;
        placementPoseIsValid = false;
        SetAllPlanesActive(true);
        placementIndicator.SetActive(false);
        if (instantiatedPrefab != null)
        {
            instantiatedPrefab.transform.position = Asiobia;
        }
    }

    void SetAllPlanesActive(bool value)
    {
        foreach (ARPlane plane in arOrigin.GetComponent<ARPlaneManager>().trackables)
            plane.gameObject.SetActive(value);
    }

}