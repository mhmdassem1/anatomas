using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Photon.Pun;
using Photon.Realtime;

using SO;
using System;

public class AnatomyModelHandler : MonoBehaviour
{
    PhotonView photonview;
    public float speed;

    Vector3 prevPos;
    Vector3 DeltaPos;
    bool isRotating = false;
    float initialDistance;
    Vector3 initialScal;
    Button Show;
    Button Hide;
    public IntSO GameLevel;
    int PrevGameLevel;
    GameObject Parent;

    private void Awake()
    {
        Parent = transform.GetChild(0).GetChild(0).gameObject;
        if (PhotonNetwork.IsMasterClient)
        {
            Show = GameObject.FindGameObjectWithTag("Show").GetComponent<Button>();
            Hide = GameObject.FindGameObjectWithTag("Hide").GetComponent<Button>();
        }

        isRotating = false;
        speed = 15;
        prevPos = Vector3.zero;
        DeltaPos = Vector3.zero;
        photonview = GetComponent<PhotonView>();
        GameLevel.Subscripe(OnChange);
       
    }

    private void Start()
    {
        if (PhotonNetwork.IsMasterClient)
        {

            if (GameLevel.Value == 0)
            {
                Show.interactable = false;
            }
            else if (GameLevel.Value == Parent.transform.childCount - 2)
            {
                Hide.interactable = false;
            }
            else
            {
                Show.interactable = true;
                Hide.interactable = true;
            }
        }
        
        OnChange(null, null);
    }


    void Update()
    {
#if UNITY_EDITOR
        if (PhotonNetwork.IsMasterClient)
        {
            transform.Rotate(0, .1f, 0);


        }
#else
        if (PhotonNetwork.IsMasterClient)
        {

        /// rotation Left and right
            if (Input.touchCount == 1)
            {
                if (!isRotating)
                {
                    prevPos = Input.GetTouch(0).position;
                    isRotating = true;

                    return;
                }
                DeltaPos = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0) - prevPos;
                transform.Rotate(transform.up, -Vector3.Dot(DeltaPos, Camera.main.transform.right), Space.World);
                prevPos = Input.GetTouch(0).position;
            }
            else if(Input.touchCount >= 2)
            {
                var FirstTouch = Input.GetTouch(0);
                var SecondTouch = Input.GetTouch(1);

                if (FirstTouch.phase == TouchPhase.Ended || FirstTouch.phase == TouchPhase.Canceled || SecondTouch.phase == TouchPhase.Ended || SecondTouch.phase == TouchPhase.Canceled) return;

                if(FirstTouch.phase == TouchPhase.Began || SecondTouch.phase == TouchPhase.Began)
                {
                    initialDistance = Vector2.Distance(FirstTouch.position, SecondTouch.position);
                    initialScal = transform.localScale;
                }
                else
                {
                    var CurrentDistance = Vector2.Distance(FirstTouch.position, SecondTouch.position);
                    if (Mathf.Approximately(CurrentDistance, 0)) return;

                    var factor = CurrentDistance / initialDistance;
                    transform.localScale =initialScal*factor;
                }


            }
            else if(Input.touchCount ==0)
            {
                isRotating = false;
            }
        }

#endif

    }

    private void OnChange(object sender, EventArgs e)
    {
        if (GameLevel.Value == 0)
        {
            Show.interactable = false;
        }

        else if (GameLevel.Value == Parent.transform.childCount - 2)
        {
            Hide.interactable = false;
        }
        else
        {
            Show.interactable = true;
            Hide.interactable = true;
        }

        for (int i = 0; i < Parent.transform.childCount; i++)
        {
            Parent.transform.GetChild(i).gameObject.SetActive(i >= GameLevel.Value);
            //SetEnabledOfColiders(Parent.transform.GetChild(i), i == GameLevel.Value);
        }
    }

    private void SetEnabledOfColiders(Transform transform, bool isEnabled)
    {
        var coliders = transform.GetComponentsInChildren<Collider>(true);
        for (int i = 0; i < coliders.Length; i++)
        {
            coliders[i].enabled = isEnabled;
        }
    }
}
