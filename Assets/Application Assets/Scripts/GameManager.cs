﻿/*
 * Copyright (c) 2019 Razeware LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish, 
 * distribute, sublicense, create a derivative work, and/or sell copies of the 
 * Software in any work that is designed, intended, or marketed for pedagogical or 
 * instructional purposes related to programming, coding, application development, 
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works, 
 * or sale is expressly withheld.
 *    
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

using UnityEngine;
using UnityEngine.SceneManagement;

using Photon.Realtime;
using SO;
using UnityEngine.XR.ARFoundation;

namespace Photon.Pun.Demo.PunBasics
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        public GameObject SessionOrigin;

        private GameObject AnatomyModel;
        private GameObject player1;
        private GameObject player2;

        private void Awake()
        {
            SessionOrigin = Camera.main.gameObject;
        }

        // Start Method
        void Start()
        {
            if (!PhotonNetwork.IsConnected) // 1
            {
                SceneManager.LoadScene("Launcher");
                return;
            }

            if (PlayerManager.LocalPlayerInstance == null)
            {
                if (PhotonNetwork.IsMasterClient) // 2
                {
                    Debug.Log("Instantiating Player 1");
                    // 3
                    player1 = PhotonNetwork.Instantiate("Player", Vector3.zero, Quaternion.identity, 0);
                    //player1.transform.parent = SessionOrigin.transform;
                    //player1.transform.position = Vector3.zero;
                    //player1.transform.rotation = Quaternion.identity;

                    // 4
                    AnatomyModel = PhotonNetwork.Instantiate("Anatomy Model", Vector3.zero, Quaternion.identity, 0);


                }
                else // 5
                {
                    Debug.Log("Instantiating Player 2");
                    player2 = PhotonNetwork.Instantiate("Player", Vector3.zero, Quaternion.identity, 0);
                    //player2.transform.parent = SessionOrigin.transform;
                    //player2.transform.position = Vector3.zero;
                    //player2.transform.rotation = Quaternion.identity;
                }
            }
        }

        private void Update()
        {
            // Check if Back was pressed this frame
            if (Input.GetKeyDown(KeyCode.Escape)) 
            {
                QuitRoom();
            }
        }

        // Photon Methods
        public override void OnPlayerLeftRoom(Player other)
        {
            Debug.Log("OnPlayerLeftRoom() " + other.NickName);
            if (other.IsMasterClient)
            {
                PhotonNetwork.LoadLevel("Launcher");
            }

        }

        // healper Method
        public void QuitRoom()
        {
            PhotonNetwork.LoadLevel("Launcher");
            PhotonNetwork.LeaveRoom();

        }
    }
}
