using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LaserPointer : MonoBehaviour
{
    [SerializeField]
    float maxLaserDistance;

    LineRenderer lineRenderer;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        Vector3 secondPointPosition;
        if (Physics.Raycast(ray, out RaycastHit hitInfo, maxLaserDistance))
        {
            secondPointPosition = hitInfo.point;
        }
        else
        {
            secondPointPosition = transform.position + transform.forward * maxLaserDistance;
        }

        lineRenderer.SetPosition(1, secondPointPosition);
        lineRenderer.SetPosition(0, transform.position);
    }

    public void ShowLineRenderer(bool val)
    {
        gameObject.SetActive(val);
    }
}
