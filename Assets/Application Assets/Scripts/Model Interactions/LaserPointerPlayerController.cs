using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SO;
using System;
using DigitalRubyShared;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class LaserPointerPlayerController : MonoBehaviour
{
    [SerializeField]
    BoolSO enableLaserPointerFlag;

    [SerializeField]
    LaserPointer laserPointerInstance;

    [SerializeField]
    LayerMask notesLayerMask;

    public PhotonView photonView;

    int? lastHitId;

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
        enableLaserPointerFlag.Subscripe(EnableOrDisableLaser);
    }

    private void Start()
    {
        laserPointerInstance.ShowLineRenderer(false);
    }

    private void EnableOrDisableLaser(object sender, EventArgs e)
    {
        if (photonView.IsMine)
        {
            if (enableLaserPointerFlag)
            {
                OpenNote();
                laserPointerInstance.ShowLineRenderer(true);
            }
            else
            {
                lastHitId = null;
                laserPointerInstance.ShowLineRenderer(false);
            }
        }
    }

    void OpenNote()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out RaycastHit hitInfo, float.MaxValue, notesLayerMask))
        {
            if (hitInfo.collider.GetInstanceID() != lastHitId)
            {
                NoteMarker noteMarker = hitInfo.collider.GetComponentInParent<NoteMarker>();
                noteMarker.ShowOrHideMessage();
                lastHitId = hitInfo.collider.GetInstanceID();
            }
        }
    }
}
