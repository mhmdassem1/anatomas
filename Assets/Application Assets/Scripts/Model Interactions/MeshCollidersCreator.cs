using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class MeshCollidersCreator : MonoBehaviour
{
    [SerializeField, ContextMenu("Create Colliders On Meshes")]
    void AddCollidersOnMeshes()
    {
        RemoveCollidersFromMeshes();

        IEnumerable<MeshRenderer> modelMeshes = GetComponentsInChildren<MeshRenderer>();
        foreach(var mesh in modelMeshes)
        {
            mesh.gameObject.AddComponent<MeshCollider>();
        }

#if UNITY_EDITOR
        EditorUtility.SetDirty(gameObject);
#endif
    }

    [SerializeField, ContextMenu("Remove Colliders From Meshes")]

    void RemoveCollidersFromMeshes()
    {
        IEnumerable<MeshCollider> modelMeshes = GetComponentsInChildren<MeshCollider>();
        foreach (var mesh in modelMeshes)
        {
            DestroyImmediate(mesh);
        }

#if UNITY_EDITOR
        EditorUtility.SetDirty(gameObject);
#endif
    }
}
