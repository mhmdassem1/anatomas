using ExitGames.Client.Photon;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using Text = TMPro.TMP_Text;

public struct NoteData
{
    public string nickname;
    public Vector3 direction;
    public string message;

    public override string ToString()
    {
        return $"{nickname}: {message}";
    }
}

[RequireComponent(typeof(PhotonView))]
public class NoteMarker : MonoBehaviour, IPunObservable
{
    [SerializeField]
    Text text;
    [SerializeField]
    GameObject pinHead;
    [SerializeField]
    LineRenderer pinTail;
    [SerializeField]
    GameObject messageObject;

    PhotonView photonView;

    bool serializingData;

    NoteData noteData = new NoteData();

    bool messageOpen = false;

    void UpdateNoteView()
    {
        pinHead.transform.position = transform.position + noteData.direction * 0.1f;
        messageObject.transform.position = transform.position + noteData.direction * 0.25f;
        text.text = $"{noteData.nickname}:\n\n{SplitToLines(noteData.message, 10)}";
        HideMessage();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(serializingData);
            stream.SendNext(noteData.nickname);
            stream.SendNext(noteData.message);
            stream.SendNext(noteData.direction);
        }
        else
        {
            bool reading = (bool)stream.ReceiveNext();
            if (!reading) return;
            noteData = new NoteData()
            {
                nickname = (string)stream.ReceiveNext(),
                message = (string)stream.ReceiveNext(),
                direction = (Vector3)stream.ReceiveNext(),
            };
            UpdateNoteView();
        }
    }

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }

    public void SetData(NoteData noteData)
    {
        this.noteData = noteData;
        serializingData = true;
        UpdateNoteView();
    }

    string SplitToLines(string stringToSplit, int maximumLineLength)
    {
        return Regex.Replace(stringToSplit, @"(.{1," + maximumLineLength + @"})(?:\s|$)", "$1\n");
    }

    private void Update()
    {
        pinTail.SetPosition(0, transform.position);
        pinTail.SetPosition(1, pinHead.transform.position);
    }

    public void ShowOrHideMessage()
    {
        if (messageOpen)
        {
            HideMessage();
            return;
        }

        messageObject.SetActive(true);
        pinHead.SetActive(false);
        messageOpen = true;

        Invoke(nameof(HideMessage), 20);
    }

    public void HideMessage()
    {
        messageObject.SetActive(false);
        pinHead.SetActive(true);
        messageOpen = false;
        CancelInvoke();
    }
}
