using DigitalRubyShared;
using ExitGames.Client.Photon;
using Photon.Pun;
using SO;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WritingNotesPlayerController : MonoBehaviour
{
    [SerializeField]
    StringSO lastEnteredNote;

    [SerializeField]
    LayerMask layerMask;

    [SerializeField]
    BoolSO isSettingNoteLocation;

    PhotonView photonView;

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }

    private void CreateNote()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out RaycastHit hitInfo, float.MaxValue, layerMask))
        {
            NoteData noteData = new NoteData()
            {
                nickname = PhotonNetwork.NetworkingClient.NickName,
                direction = (transform.position - hitInfo.point).normalized,
                message = lastEnteredNote
            };
            GameObject noteObject = PhotonNetwork.Instantiate("Note Marker", hitInfo.point, Quaternion.identity);
            NoteMarker noteMarker = noteObject.GetComponent<NoteMarker>();
            noteMarker.SetData(noteData);
            Transform anatomyModelTransform = GameObject.FindGameObjectWithTag("AnatomyModel").transform;
            noteMarker.transform.SetParent(anatomyModelTransform, true);
        }
    }

    private void Update()
    {
        if (photonView.IsMine && isSettingNoteLocation)
        {
            if (Input.GetMouseButtonDown(0))
            {
                CreateNote();
                isSettingNoteLocation.Value = false;
            }
        }
    }
}
