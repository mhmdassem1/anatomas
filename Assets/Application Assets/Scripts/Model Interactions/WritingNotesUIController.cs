using SO;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InputField = TMPro.TMP_InputField;


public class WritingNotesUIController : MonoBehaviour
{
    [SerializeField]
    InputField inputField;

    [SerializeField]
    StringSO lastEnteredNote;

    [SerializeField]
    BoolSO IsEnteringNotes;

    [SerializeField]
    List<GameObject> notesControlsUI;

    private void Start()
    {
        IsEnteringNotes.Subscripe(ShowOrHideNotesControls);
    }

    private void ShowOrHideNotesControls(object sender, EventArgs e)
    {
        notesControlsUI.ForEach(noteControl => noteControl.SetActive(IsEnteringNotes));
    }

    public void SetNoteValue()
    {
        lastEnteredNote.Value = "" + inputField.text;
        inputField.text = "";
    }
}
