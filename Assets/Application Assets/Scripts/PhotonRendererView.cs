using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SO;

public class PhotonRendererView : MonoBehaviour, IPunObservable
{
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(gameObject.activeInHierarchy);
        }
        else
        {
            gameObject.SetActive((bool)stream.ReceiveNext());
        }
    }
}
