using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using Photon.Pun;

public class PlayerHandler : MonoBehaviour
{
    // Start is called before the first frame update
    public TMP_Text PlayerName;
    public SO.GameObjectSO myPlayer;
    public SO.BoolSO isRaiseMyHand;
    PhotonView photonView;
    Transform mainCameraTransform;
    public GameObject model;
    public GameObject handCanvas;


    private void Awake()
    {
        photonView = gameObject.GetComponent<PhotonView>();
        mainCameraTransform = Camera.main.transform;
    }

    void Start()
    {
        if (photonView.IsMine)
        {
            myPlayer.Value = gameObject;
            model.SetActive(false);
        }
        else
        {
            model.SetActive(true);
        }

        handCanvas.SetActive(false);

        gameObject.name = photonView.Owner.NickName;
        PlayerName.text = photonView.Owner.NickName;
    }
    private void Update()
    {
        if (photonView.IsMine)
        {
            transform.position = mainCameraTransform.transform.position;
            transform.rotation = mainCameraTransform.transform.rotation;

            if (isRaiseMyHand)
            {
                handCanvas.SetActive(true);
            }
            else
            {
                handCanvas.SetActive(false);
            }
        }
    }

    private void FixedUpdate()
    {
        if (photonView.IsMine)
        {
            transform.position = mainCameraTransform.transform.position;
            transform.rotation = mainCameraTransform.transform.rotation;
        }
    }
    private void LateUpdate()
    {
        if (photonView.IsMine)
        {
            transform.position = mainCameraTransform.transform.position;
            transform.rotation = mainCameraTransform.transform.rotation;
        }
    }
}
