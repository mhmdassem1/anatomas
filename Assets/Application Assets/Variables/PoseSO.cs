using SO;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SO {

    [CreateAssetMenu(fileName = "PoseSO", menuName = "SO/Variables/Pose")]

    public class PoseSO : VariableSO<Pose>
    {
        public override void SetValue(string value)
        {
            Value = JsonUtility.FromJson<Pose>(value);
        }

        public override string ToString(string format, IFormatProvider formatProvider)
        {
            return JsonUtility.ToJson(Value);   

        }

      
    }
}

