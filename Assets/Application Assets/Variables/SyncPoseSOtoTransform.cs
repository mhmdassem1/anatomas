using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SO;

public class SyncPoseSOtoTransform : MonoBehaviour
{
    public PoseSO origin;
    private void Update()
    {
        transform.position = origin.Value.position;
    }

}
