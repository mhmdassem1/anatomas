using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SO;



public class SyncTransformToSOVar : MonoBehaviour
{

    public PoseSO ModelPose;
    
    
    // Update is called once per frame
    void Update()
    {
        Pose pos = new Pose(transform.position,transform.rotation);
        ModelPose.Value = pos;

    }
}
