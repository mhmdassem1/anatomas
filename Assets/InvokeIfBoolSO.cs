using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InvokeIfBoolSO : MonoBehaviour
{

    public SO.BoolSO value;

    [Header("check when")]
    public bool OnStart;
    public bool OnValueChange;
    [Header("actions")]
    public UnityEvent OnTrue;
    public UnityEvent OnFalse;



    private void Awake()
    {
        value.Subscripe(OnChange);
    }

    private void OnChange(object sender, EventArgs e)
    {
        if (OnValueChange)
        {
            Check();
        }
    }

    void Start()
    {
        if (OnStart)
        {
            Check();
        }
    }



    public void Check()
    {
        if (value)
        {
            OnTrue.Invoke();
        }
        else
        {
            OnFalse.Invoke();
        }
    }


}
