using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lookAtGameObject : MonoBehaviour
{

    public SO.GameObjectSO target;
    public Vector3 rotateFactor;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (target.Value != null)
        {
            transform.LookAt(target.Value.transform);
            transform.Rotate(rotateFactor);
        }
    }
}
